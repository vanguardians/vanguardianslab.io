---
title: Support
include_footer: true
---

## Collaborator Signup
We need collaborators the most! If you would like to collaborate on VNGRD, signup [here](https://forms.gle/m53NmxFDAhNsWsQv8) and we will schedule a call with you to get you up to speed.

## Contact
For anything else, or if you're just curious to learn more, follow us on [Twitter](https://twitter.com/vngrd_coop) or email us at [vngrdcoop@hotmail.com](mailto:vngrdcoop@hotmail.com)
