---
title: About
include_footer: true
---
## Mission
To build a platform cooperative that fosters community participation and shares wealth fairly between podcasters, community members, and VNGRD workers.

## Execution
We will form a multi-stakeholder cooperative with podcaster-members, worker-members, and community-members. Podcasters will set subscriber payment tiers, and the platform will take a predetermined percentage of their earnings. This revenue will be divided between workers, community-members, and podcasters in a fashion to be decided by the Board of Directors.

The Board of Directors will consist of an equal, odd number of seats for each ownership type. For example, three seats for podcasters, three seats for workers, and three seats for community-members. Each ownership type will hold transparent elections to vote in their representatives.

## Current State
We need more collaborators! React frontend, Django backend API, PostgreSQL database, Docker. If you are interested in working on VNGRD, please fill out [this form](https://forms.gle/m53NmxFDAhNsWsQv8) or reach out via [email](mailto:vngrdcoop@hotmail.com).
