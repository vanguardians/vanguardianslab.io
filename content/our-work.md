---
title: Our Work
include_footer: true
---
## Motivation
Tech giants and internet service providers have centralized the power of the internet. While it is absolutely worthwhile to fight against them politically/electorally, we must also build systems to circumvent them entirely (a dual power structure, if you will). More specifically, we should build software that incentivizes the distribution and balance of power among creators, users, and workers. Existing services like Patreon, Etsy, and Ebay all provide a platform for everyday, normal people to sell via their platforms. These companies act as a convenient middleman for people just trying to sell goods or content online, but there is a massive power imbalance that resides in the platform's discretion to host their creations. Eventually these companies will be replaced by platforms that are cooperatively owned and operated. VNGRD is just one media-specific implementation of this transformation.

## Strategy
VNGRD will launch as a cooperative podcasting platform for the Left in the hopes that Leftist podcasters will be convinced and begin using VNGRD. The business, social, and political interests of both VNGRD and its user base will be the same - to stay alive and sustainably grow the subscriber base while moderating Leftist content and promoting cooperation. After stablizing the initial launch, work will then begin to tailor the platform to various unions and social movements. After stablizing that, work will begin to build out the blockchain ecosystem to allow true platform independence. A more detailed strategy is outlined in the [Medium article](https://medium.com/@samp805/vngrd-cooperative-media-for-the-left-7499920455ea).

## Current State
We are using Django as the backend API, React Javascript as the frontend, PostgreSQL as the database, and Docker to containerize everything. For collberation tools we use Slack and GitLab. Very minimal functionality is currently implemented (users, posts, and authetication). We are seeking collaborators to help design and implement additional functionality. Signup using [this Google form](https://forms.gle/m53NmxFDAhNsWsQv8) if you are interested.
