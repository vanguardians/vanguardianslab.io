baseURL: vanguardians.gitlab.io
languageCode: en-us
title: VNGRD
theme: hugo-fresh
googleAnalytics: # Put in your tracking code without quotes like this: UA-XXXXXX...
# Disables warnings
disableKinds:
- taxonomy
- taxonomyTerm
markup:
  goldmark:
    extensions:
      definitionList: true
      footnote: true
      linkify: true
      strikethrough: true
      table: true
      taskList: true
      typographer: true
    parser:
      attribute: true
      autoHeadingID: true
    renderer:
      hardWraps: false
      unsafe: false
      xHTML: false

params:
  # Open graph allows easy social sharing. If you don't want it you can set it to false or just delete the variable
  openGraph: true
  # Used as meta data; describe your site to make Google Bots happy 
  description: 
  navbarlogo:
  # Logo (from static/images/logos/___)
    image: logos/vngrd-text.svg
    link: /
  font:
    name: "Open Sans"
    sizes: [400,600]
  hero:
    # Main hero title
    title: VNGRD
    # Hero subtitle (optional)
    subtitle: A cooperative podcasting platform for the Left
    # Button text
    buttontext: Change this
    # Where the main hero button links to
    buttonlink: "#"
    # Hero image (from static/images/___)
    image: logos/vngrd-logo.png
  # Customizable navbar. For a dropdown, add a "sublinks" list.
  navbar:
  - title: Home
    url: /
  - title: About
    sublinks:
      - title: Who We Are
        url: /page/who-we-are
      - title: Philosophy
        url: /page/philosophy
  - title: Our Work
    url: /page/our-work
  - title: Support
    url: /page/support
  section1:
    title: Why?
    subtitle: The motivation behind creating VNGRD
    tiles:
    - title: Tech Oligopoly
      icon: mouse-globe
      text: Too much power in too few hands
      url: /
      buttonText: Learn more
    - title: Free Speech
      icon: laptop-cloud
      text: Vetting creators and never deplatforming them
      url: /
      buttonText: Learn more
    - title: Dual Power Systems
      icon: plug-cloud
      text: Need systems removed from the interests of capital
      url: /
      buttonText: Learn more
  section2:
    title: How?
    subtitle: The strategy to get VNGRD up and running
    features:
    - title: Initial focus on podcasts
      text: There are many Leftist podcasters using Patreon to generate income. They will switch to VNGRD because it is more in-line with the Leftist movement and won't need to worry about being deplatformed. Also, focusing solely on podcasts will speed up development (though we plan to expand into all media forms eventually).
      icon: mouse-globe
      # Icon (from /images/illustrations/icons/___.svg)
    - title: Complete transparency
      text: We raise seed capital via Open Collective so everyone can see the money raised and how we use it. Once the platform is operational there will be a page detailing how much money the platform is earning and how we're using it. Transparency is critical for building trust between all parties.
      icon: mouse-globe
    - title: Traditional tech stack
      text: This platform is being built with Django and ReactJS, and will be hosted wherever is cheapest & easiest. This allows developers to speedily contribute shortening time to market. After the platform is established, we will then take measures to ween ourselves off relying on services provided by tech giants.
      icon: mouse-globe
  section3:
    title: Now?
    subtitle: What is currently being done?
    features:
    - title: Building cooperative infrastructure
      text: We have a Slack workspace for discussing ideas, GitLab for collaberative programming, and Open Collective to raise money.
      icon: mouse-globe
      # Icon (from /images/illustrations/icons/___.svg)
    - title: Developing the platform
      text: Work has begun, and very simple functionality has been implemented. We are seeking more collaberators to speed the progress along.
      icon: mouse-globe
    - title: Making promotional material
      text: This website and the Medium article will serve as landing pages. Social media accounts will be created, and a legal worker cooperative will eventually be formed.
      icon: mouse-globe
  section5: true
  footer:
    # Logo (from /images/logos/___)
    logo: vngrd-text.svg
    # Social Media Title
    socialmediatitle: Follow Us
    # Social media links (GitHub, Twitter, etc.). All are optional.
    socialmedia:
    - link: https://github.com/vngrd-coop/vngrd
      # Icons are from Font Awesome
      icon: github
    - link: https://twitter.com/samueldavidpete
      icon: twitter
    quicklinks:
      column1:
        title: "Product"
        links:
        - text: Discover features
          link: /
        - text: Why choose our product?
          link: /
        - text: Compare features
          link: /
        - text: Our roadmap
          link: /
        - text: AGB
          link: /agb
      column2:
        title: "Docs"
        links:
        - text: Get started
          link: /
        - text: User guides
          link: /
        - text: Admin guide
          link: /
        - text: Developers
          link: /
      column3:
        title: "Blog"
        links:
        - text: Latest news
          link: /blog/first
        - text: Tech articles
          link: /blog/second
